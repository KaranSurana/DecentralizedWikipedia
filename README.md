# DecentralizedWikipedia



- Decentralized Wikipedia
>- Decentralized Wikipedia aiming to improve security of the data, eliminates the Central Point of Control as well as the increases the efficiency of the web application.
>- Tech Stack Used: HTML, CSS, JavaScript, Web3 JS, Solidity
>- I have yet to upload the code on GitHub

![image](https://assets.devfolio.co/content/482d5997e36c4a69b05e21ddb84c95c3/007df670-46bb-4215-8b5f-a55a443cad9e.png)

![image](https://assets.devfolio.co/content/482d5997e36c4a69b05e21ddb84c95c3/bd178d30-4895-455a-b40c-507ea0c46ecb.png)
>- Create your own content using the autocomplete feature available in the web application.
>- HTML tags also supported for editing.

![image](https://assets.devfolio.co/content/482d5997e36c4a69b05e21ddb84c95c3/0d870cbb-d0a1-4a3d-9b48-267fb12642f2.png)

